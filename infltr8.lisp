;;; infltr8.lisp --- space combat survival horror cartridge

;; Copyright (C) 2010, 2011  David O'Toole

;; Author: David O'Toole <dto@ioforms.org>
;; Keywords: games

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Preamble

(defpackage :infltr8 
  (:use :blocky :common-lisp))

(in-package :infltr8)

(setf *screen-width* 800)
(setf *screen-height* 600)
(setf *nominal-screen-width* 800)
(setf *nominal-screen-height* 600)
(setf *window-title* "infltr8")
(setf *use-antialiased-text* nil)
(setf *default-texture-filter* :nearest) 
(setf *scale-output-to-window* t) 
(setf *frame-rate* 30)
(setf *dt* 33)
(disable-key-repeat) 

(defparameter *game-font* "sans-mono-bold-12")

;;; Colored, themeable bricks that make up the environment

(defparameter *themes* 
  '((:dec :background "DarkSlateBlue" :brick "orchid" :brick2 "hot pink" :wall "Black")
    (:tandy :background "gray5" :brick "red" :brick2 "gray30" :wall "gray20")
    (:vax :background "gray5" :brick "gray50" :brick2 "gray30" :wall "gray20")
    (:command :background "black" :brick "gold" :brick2 "gray40" :wall "gray20")
    (:surround :background "DarkOliveGreen" :brick "GreenYellow" :brick2 "gray40" :wall "gray20")
    (:maynard :background "saddle brown" :brick "DarkOrange" :brick2 "gray40" :wall "gray20")
    (:zerk :background "black" :brick "maroon2" :brick2 "cyan" :wall "black")))

(defun random-theme () (random-choose (mapcar #'car *themes*)))

(defparameter *theme* :tandy)

(defun theme-color (&optional (part :brick))
  (let ((theme (assoc *theme* *themes*)))
    (getf (rest theme) part)))

(define-block brick 
  :tags '(:brick)
  :part :brick
  :collision-type :passive
  :color (theme-color))

(define-method layout brick ())

(define-method bounding-box brick ()
  ;; reduce bounding box by a tiny amount, to prevent adjacent bricks
  ;; from resting contact
  (with-field-values (x y height width) self
    (values (+ 0.1 y) (+ 0.1 x)
	    (+ -0.1 x width)
	    (+ -0.1  y height))))

(define-method damage brick (points) nil)

(define-method initialize brick (&optional color)
  (initialize%super self)
  (resize self 16 16)
  (setf %color color))

(defmacro defbrick (name &body body)
  `(define-block (,name :super "XALCYON:BRICK")
     ,@body))

(defun is-brick (thing)
  (and (blockyp thing)
       (has-tag thing :brick)))

(define-method draw brick ()
  (draw-box %x %y %width %height :color (theme-color %part)))

;;; Sparkle clouds

(define-block spark 
  :width 3 :height 3
  :collision-type nil)

(define-method initialize spark ()
  (later 0.8 (exit self)))

(define-method draw spark ()
  (dotimes (n 3)
    (draw-box (+ %x (random 20))  (+ %y (random 20)) (+ 1 (random 5)) (+ 1 (random 4))
	      :color (random-choose '("white" "cyan" "yellow" "magenta")))))

(define-method update spark ()
  (move-toward self (random-direction) (+ 4 (random 6))))

(defun make-sparks (x y &optional (n 5))
  (dotimes (z n)
    (add-object (world) 
		(new 'spark) 
		(+ x (random 30)) (+ y (random 30)))))

;;; An enemy bullet 

(define-block bullet 
  :height 4 :width 4
  :clock 300
  :speed 4
  :tags '(:bullet))

(define-method update bullet ()
  (forward self %speed)
  (decf %clock)
  (when (zerop %clock)
    (destroy self)))

(define-method collide bullet (thing)
  (when (not (has-tag thing :enemy))
    (if (is-a 'trail thing)
	(progn 
	  (play-sound self "bonux3")
	  (destroy self))
	(unless (is-a 'bullet thing)
	  (when (has-method :damage thing)
	    (damage thing 1)
	    (destroy self))))))

(define-method initialize bullet (heading)
  (initialize%super self)
  (setf %heading heading))

(define-method draw bullet ()
  (draw-box %x %y 4 2 :color (random-choose '("yellow" "red"))))

;;; Sentinel enemy

(define-block sentinel 
  (hit-points :initform 1)
  (direction :initform (random-choose '(:up :down :right :left)))
  (tags :initform '(:sentinel :enemy))
  (image :initform "mtron-1.png"))

(define-method choose-new-direction sentinel ()
  (setf %direction
	(if (= 0 (random 16))
	    ;; occasionally choose a random dir
	    (nth (random 3)
		 '(:up :down :right :left :upright :downleft :downright :upleft))
	    ;; otherwise turn left
	    (getf '(:up :left :left :down :down :right :right :up)
		  (or %direction :up)))))

(define-method update sentinel ()
  (percent-of-time 32 (setf %image (random-choose '("mtron-1.png" "mtron-2.png" "mtron-3.png"))))
  (if (< (distance-to-player self) 160)
      (progn (setf %direction (direction-to-player self))
	     (percent-of-time 40 (move self (heading-to-player self) 1))
	     (percent-of-time 2
	       (fire self (+ (heading-to-player self) 10))))
			     ;; a little randomness to sometimes lead player
;	       (later 2 (move-toward self %direction 0.2))
	       ;; (play-sound self (defresource :name "magenta-alert"
	       ;; 				 :type :sample :file "magenta-alert.wav" 
	       ;; 				 :properties (:volume 40)))))
      (move-toward self %direction 2)))

(define-method collide sentinel (thing)
  (when (is-a 'agent thing)
    (damage thing 1))
  (restore-location self)
  (choose-new-direction self))

(define-method damage sentinel (points)
  (play-sample (defresource :name "boom-1.wav" :file "boom-1.wav" :type :sample :properties (:volume 30)))
  (make-sparks (- %x 16) (- %y 16))
  ;; (play-sound self (defresource :name "xplod"
  ;; 			    :type :sample :file "xplod.wav" 
  ;; 			    :properties (:volume 60)))
;  (play-sound self (random-choose *slam-sounds*))
  (destroy self))

(define-method fire sentinel (direction)
  (play-sample (defresource :name "radar.wav" :file "radar.wav" :type :sample :properties (:volume 30)))
  (multiple-value-bind (x y) (center-point self)
    (add-object (world) (new 'bullet (heading-to-player self)) x y)))

;;; A bouncing ball to break bricks with

(defun is-ball (thing)
  (has-tag thing :ball))

(defparameter *bounce-sounds*
  (defresource 
      (:name "boop1" :type :sample :file "boop1.wav" :properties (:volume 20))
      (:name "boop2" :type :sample :file "boop2.wav" :properties (:volume 20))
      (:name "boop3" :type :sample :file "boop3.wav" :properties (:volume 20))))

(define-block ball 
  :height 5 :width 5
  :clock 200
  :speed 6
  :tags '(:ball)
  :direction :right)

(define-method update ball ()
  (move-toward self %direction %speed)
  (decf %clock)
  (when (zerop %clock)
    (destroy self)))

(define-method collide ball (thing)
  ;; take a thing-specific action
  (unless (or (is-a 'trail thing) 
	      (is-a 'agent thing))
    (when (has-method :damage thing)
      (damage thing 1)
      (destroy self))))

(define-method initialize ball (direction)
  (assert (keywordp direction))
  (initialize%super self)
  (setf %direction direction))

(define-method draw ball ()
  (draw-box %x %y 4 2 :color (random-choose '("white" "cyan"))))

;;; The player, a secret agent

(defun is-agent (thing)
  (and (blockyp thing)
       (has-tag thing :agent)))

(defparameter *maximum-hp* 100)
(defparameter *maximum-oxygen* 100)
(defparameter *normal-pulse-rate* 12)
(defparameter *normal-respiration-rate* 10)
(defparameter *elevated-pulse-rate* 30)
(defparameter *high-pulse-rate* 50)

(defparameter *respiration-sounds* 
  (defresource
      (:name "respiration-1.wav" :type :sample :file "respiration-1.wav" :properties (:volume 20))
      (:name "respiration-2.wav" :type :sample :file "respiration-2.wav" :properties (:volume 20))
      (:name "respiration-3.wav" :type :sample :file "respiration-3.wav" :properties (:volume 20))
      (:name "respiration-4.wav" :type :sample :file "respiration-4.wav" :properties (:volume 20))
      (:name "respiration-5.wav" :type :sample :file "respiration-5.wav" :properties (:volume 20))
      (:name "respiration-6.wav" :type :sample :file "respiration-6.wav" :properties (:volume 20))))

(define-block agent 
  (dead :initform nil)
  (item :initform nil)
  (ready :initform t)
  (ready-clock :initform 0)
  (running-clock :initform 0)
  (respiration-rate :initform *normal-respiration-rate*)
  (respiration-clock :initform 0)
  (pulse-rate :initform *normal-pulse-rate*)
  (pulse-sound :initform "pulse-low.wav")
  (pulse-clock :initform 0)
  (breaths :initform 0)
  (hp :initform *maximum-hp*)
  (oxygen :initform *maximum-oxygen*)
  (direction :initform :up)
  ;; we want to catch the beginning of firing, even if the input
  ;; polling in `update' misses it. (see below)
;  (default-events :initform '(((:space) (fire))))
  (image :initform "agent.png")
  ;; (height :initform 16)
  ;; (width :initform 16)
  (energy :initform 4)
  (tags :initform '(:agent))
  (speed :initform 3))

(defparameter *agent-running-images* '("agent-run-1.png" "agent-run-2.png" "agent-run-3.png"))

;; (define-method draw agent ()
;;   (draw%super self)
;;   ;; indicate firing direction with a little dot
;;   (when (not %dead)
;;     (multiple-value-bind (x0 y0) (center-point self)
;;       (multiple-value-bind (x y) (step-in-direction x0 y0 %direction 4)
;; 	(draw-circle x y 3 :color "red" :type :solid)))))

(define-method move agent (direction)
  (unless (holding-shift)
    (move-toward self direction %speed)))

(define-method fire agent (&optional direction)
  (play-sample (defresource :name "vadar.wav" :file "vadar.wav" :type :sample :properties (:volume 30)))
  (drop self (new 'ball (or direction %direction)) 10 10))

(define-method damage agent (points)
  (when (not %dead)
    (play-sample "boom-1.wav") 
    (play-music "aura-2.ogg")
    (setf %dead t)
    (change-image self (defresource :name "skull" :type :image :file "skull.png"))))

(define-method collide agent (thing)
  (when (is-a 'brick thing)
    (restore-location self)))

(defun holding-down-arrow ()
  (or (keyboard-down-p :kp2)
      (keyboard-down-p :down)))

(defun holding-up-arrow ()
  (or (keyboard-down-p :kp8)
      (keyboard-down-p :up)))

(defun holding-left-arrow ()
  (or (keyboard-down-p :kp4)
      (keyboard-down-p :left)))

(defun holding-right-arrow ()
  (or (keyboard-down-p :kp6)
      (keyboard-down-p :right)))

(defun holding-fire ()
  (keyboard-down-p :z))

(define-method aim agent (dir)
  (setf %direction dir))

(define-method update agent ()
  (when (not %dead)
    ;; update weapon readiness
    (when (plusp %ready-clock)
      (decf %ready-clock))
    ;; update running man animation
    (when (plusp %running-clock)
      (decf %running-clock))
    ;; update respiration 
    (when (plusp %respiration-clock)
      (decf %respiration-clock))
    (when (zerop %respiration-clock)
      (setf %respiration-clock (truncate (seconds->frames (/ 60 %respiration-rate))))
      (incf %breaths)
      (play-sample (nth (mod %breaths (length *respiration-sounds*))
			*respiration-sounds*)))
    ;; update pulse 
    (when (plusp %pulse-clock)
      (decf %pulse-clock))
    (when (zerop %pulse-clock)
      (setf %pulse-clock (truncate (seconds->frames (/ 60 %pulse-rate))))
      (play-sample %pulse-sound))
    (setf %image "agent.png")
    ;; update inputs and movement
    (let ((direction
	    (cond 
	      ((holding-down-arrow) :down)
	      ((holding-up-arrow) :up)
	      ((holding-left-arrow) :left)
	      ((holding-right-arrow) :right))))
      ;; are we moving?
      (when direction
	(move-toward self direction %speed)
	;; update running animation
	(setf %image (nth (truncate (/ %running-clock 3.33))
			  *agent-running-images*))
	(when (zerop %running-clock)
	  (setf %running-clock 10)))
      ;; are we firing?
      (if (holding-fire)
	  (when (zerop %ready-clock)
	    (setf %ready-clock 8)
	    (fire self %direction))
	  ;; no, so just aim in the direction we were moving
	  (when direction 
	    (aim self direction))))))

;;; The spaceship

(define-world spaceship
  (background-color :initform (theme-color :background))
  (window-scrolling-speed :initform 2)
  (grid-size :initform 16)
  (grid-width :initform 64)
  (grid-height :initform 64))

(define-block spaceship-turtle)

(define-method draw-wall spaceship-turtle (segments &optional (size 32))
  (dotimes (n segments)
    (let ((brick (new 'brick)))
      (drop self brick)
      (resize brick size size)
      (forward self (+ size 0.02)))))

(define-method draw-square spaceship-turtle (size)
  (dotimes (n 4)
    (draw-wall self size)
    (turn-right self 90)))

(define-method skip-wall spaceship-turtle (segments)
  (forward self (+ 0.02 (* 32 segments))))

(define-method draw-room spaceship-turtle (size)
  (dotimes (n 4)
    (draw-wall self (- size 2))
    (skip-wall self 2)
    (draw-wall self 2)
    (turn-right self 90)))
    
(define-method run spaceship-turtle ()
  (draw-square self 31))
  ;; (skip-wall self 10)
  ;; (turn-right self 90)
  ;; (skip-wall self 10)
  ;; (turn-left self 90)
  ;; (draw-room self 10)
  ;; (turn-left self 90)
  ;; (skip-wall self 3)
  ;; (draw-room self 6))

(define-method draw-overlays spaceship ()
  (when %player 
    (multiple-value-bind (x y) (center-point %player)
      (draw-image "light-mask-1.png" 
		  (- x 1000)
		  (- y 1000)
		  :width 2000
		  :width 2000)))

  ;; heads up display
  (multiple-value-bind (top left right bottom)
      (window-bounding-box self)
    (with-field-values (hp oxygen item) %player
	(let* ((font *game-font*)
	       (line-height (font-height font))
	       (x (+ left (dash 5)))
	       (y (- bottom line-height (dash 2)))
	       (label (format nil "HP: ~3d  OXYGEN: ~3d  | movement: arrow keys | fire: Z | action: X"
			      hp oxygen)))
	  ;; draw hud background
	  (draw-box left (- y 6) *gl-screen-width* (* 4 line-height) :color "black")
	  (draw-string label 
	  	       (+ x (dash)) y 
	  	       :color "white"
	  	       :font *game-font*)))))
	  ;; ;; draw life bar 
	  ;; (draw-box x y bar-width line-height :color "gray30")
	  ;; (when (plusp energy)
	  ;;   (draw-box x y (* 1.2 energy) line-height 
	  ;; 	      :color (cond 
	  ;; 		       ((>= energy 85) "chartreuse")
	  ;; 		       ((>= energy 70) "yellow")
	  ;; 		       ((>= energy 40) "orange")
	  ;; 		       ((>= 20 energy) "red")
	  ;; 		       (t "orange"))))
	  ;; ;; show stats

;;; Startup
    
(defun infltr8 ()
  (let ((agent (new 'agent))
	(spaceship (new 'spaceship :name "demo"))
	(turtle (new 'spaceship-turtle)))
    (with-world spaceship
	(move-window-to (world) 0 0)
      ;; draw the level using the turtle defined above
      (add-object (world) turtle)
      (run turtle)
      (destroy turtle)
      ;; add player and get moving
      (set-player (world) agent)
      (add-object (world) agent 110 110)
      (dotimes (n 5)
	(add-object (world) (new 'sentinel) (random 1000) (random 1000)))
      (dotimes (n 30)
	(let ((brick (new 'brick)))
	  (add-object (world) brick (random 1300) (random 1300))
	  (resize brick (+ 20 (random 120)) (+ 60 (random 120)))))
      (bind-event (world) '(:r :control) :reset)
      (halt-music)
      (bind-event (world) '(:m :control) :alert)
      (start (world)))))

       ;; (play-music (random-choose *soundtrack*) :loop t))))

(define-method alert spaceship ()
  (play-music (defresource :name "aura-1.ogg" :file "aura-1.ogg" :type :music :properties (:volume 50)))
  (with-fields (respiration-rate pulse-rate pulse-sound) %player
    (setf respiration-rate 16)
    (setf pulse-rate 18)
    (setf pulse-sound "pulse-normal.wav")))

(define-method reset spaceship ()
  (setf *blocks* nil)
  (infltr8))

;;; xong.lisp ends here
